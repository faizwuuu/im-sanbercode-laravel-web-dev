<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("Shaun");

echo "Nama Binatang : " . $sheep->namabinatang . "<br>";
echo "Jumlah kaki : " . $sheep->legs . "<br>"; 
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";
echo "<br>"; 

$kodok = new frog("buduk");
echo "Nama Binatang : " . $kodok->namabinatang . "<br>";
echo "Jumlah Kaki : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump . "<br>";
echo "<br>"; 

$apeee = new ape("Kera Sakti");
echo "Nama Binatang : " . $apeee->namabinatang . "<br>";
echo "Jumlah Kaki : " . $apeee->legs . "<br>";
echo "Cold Blooded : " . $apeee->cold_blooded . "<br>";
echo "Yell : " . $apeee->yell . "<br>";
?>