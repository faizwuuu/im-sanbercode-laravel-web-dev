<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function send(Request $request)
    {
        $namadepan = $request['fname'];
        $namabelakang = $request['lname'];

        return view('page.home', ['namaDepan' => $namadepan, 'namaBelakang' => $namabelakang]);
    }
}
