<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'utama']);
Route::get('/reg', [RegisterController::class, 'register']);

Route::post('/welcome', [RegisterController::class, 'send']);

Route::get('/data-table', function(){
    return view('page.data-table');
});

Route::get('/table', function(){
    return view('page.table');
});

// CRUD Kategori

//CREATE
//Form Tambah Kategori
Route::get('/cast/create', [CastController::class, 'create']);
//Buat Kirim data ke database
Route::post('/cast', [CastController::class, 'store']);

//READ
//Buat Read Data
Route::get('/cast', [CastController::class, 'index']);
// Detail Kategori
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//UPDATE
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//Update Data ke Database berdasarkan ID
Route::put('/cast/{cast_id}', [CastController::class, 'update']);


//DELETE

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);