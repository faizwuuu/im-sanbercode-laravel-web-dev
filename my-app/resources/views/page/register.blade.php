    @extends('layout.master')
    @section('title')
    Test
    @endsection
    @section('sub-title')
    Tess
    @endsection
    @section('content')

    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="fname"> <br> <br>
        <label>Last Name</label><br>
        <input type="text" name="lname"> <br> <br>
        <label>Gender</label><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br> <br>
        <label>Nationality</label><br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">Spanish</option>
            <option value="">Japanese</option>
            <option value="">Korean</option>
        </select> <br> <br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="skill">Bahasa Indonesia<br>
        <input type="checkbox" name="skill">English<br>
        <input type="checkbox" name="skill"> Other <br> <br>
        <label>Bio:</label><br>
        <textarea cols="15" rows="5"></textarea><br><br>

        <input type="submit" value="Kirim">
    </form>
    @endsection