<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        /* 
            Soal No 1 
            Looping I Love PHP
            Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
            dan Looping yang ke dua menurun (Descending). 

            Output: 
            LOOPING PERTAMA
            2 - I Love PHP
            4 - I Love PHP
            6 - I Love PHP
            8 - I Love PHP
            10 - I Love PHP
            12 - I Love PHP
            14 - I Love PHP
            16 - I Love PHP
            18 - I Love PHP
            20- I Love PHP
            LOOPING KEDUA
            20 - I Love PHP
            18 - I Love PHP
            16 - I Love PHP
            14 - I Love PHP
            12 - I Love PHP
            10 - I Love PHP
            8 - I Love PHP
            6 - I Love PHP
            4 - I Love PHP
            2 - I Love PHP
        */
        // Lakukan Looping Di Sini
        echo "<h4>Looping 1</h4>";
        for($i = 2; $i<=20; $i+=2){
            echo $i . " I Love php <br>";
        }

        echo "<h4>Looping 2</h4>";
        $x = 20;
        do {
            echo "$x - I love PHP <br>";
            $x-=2;
        } while ($x >= 1);


        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        echo "<br>";
        foreach($numbers as $value){
            $items[] = $value%5;
        }

        echo "Array sisa bagi angka 5 adalah : ";
        print_r($items);

        // Lakukan Looping di sini

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        // Output: 
        foreach($items as $arrayindex){
            $biodata = [
                'id' => $arrayindex[0],
                'Nama' => $arrayindex[1],
                'Harga' => $arrayindex[2],
                'Ket' => $arrayindex[3],
                'Gambar' => $arrayindex[4]
            ];
            print_r($biodata);
            echo "<br>";
        }

        
        echo "<h3>Soal No 4 Asterix </h3>";
        echo "Asterix: ";
        echo "<br>";    
        $star=5;
        for($a=$star;$a>0;$a--){
        for($b=$star;$b>=$a;$b--){
            echo "*";
        }
        echo "<br>";
        }
    
    ?>

</body>
</html>

